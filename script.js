const gameContainer = document.getElementById("game")
const reset = document.getElementById('reset')
const turns = document.getElementById('counter')
const gameSection = document.getElementById('game-section')
const gameTurns = document.getElementById('turn-status')
const winTag = document.getElementById('win-status')
const turnStatus = document.getElementById('turn-status')
const cardOpen = document.getElementsByClassName('cardOpen')
const bestSocre = document.getElementById('best-score')
const bestSocreStatus = document.getElementById('best-score-status')
const instruction = document.getElementById('instruction-section')

function randomNumberOfCard() {
  const level = [4, 6, 8]
  const randomNumberIndex = Math.floor(Math.random() * 3)
  return level[randomNumberIndex]
}

function generateRandomNumberOfGifs(number) {
  const gifs = []
  for (let index = 1; index <= number; index++) {
    const gif = `url(../gifs/${index}.gif)`
    gifs.push(gif)
  }
  const doubleOfGifs = gifs.concat(gifs)
  return doubleOfGifs
}

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
  let counter = array.length

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter)

    // Decrease counter by 1
    counter--

    // And swap the last element with it
    let temp = array[counter]
    array[counter] = array[index]
    array[index] = temp
  }

  return array
}

let randomNumber = randomNumberOfCard()
let randomGifsArray = generateRandomNumberOfGifs(randomNumber)
let shuffledColors = shuffle(randomGifsArray)

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForColors(colorArray) {
  for (let color of colorArray) {
    // create a new div
    const newDiv = document.createElement("div")

    // give it a class attribute for the value we are looping over
    newDiv.classList.add("card")
    newDiv.classList.add(color)

    // call a function handleCardClick when a div is clicked on
    newDiv.addEventListener("click", handleCardClick)

    // append the div to the element with an id of game
    gameContainer.append(newDiv)
  }
}

// if local Storage don't get any value from with score key
if (localStorage.getItem('score-4') === null) {
  localStorage.setItem('score-4', Infinity)
}
if (localStorage.getItem('score-6') === null) {
  localStorage.setItem('score-6', Infinity)
}
if (localStorage.getItem('score-8') === null) {
  localStorage.setItem('score-8', Infinity)
}

let scoreType
if (randomNumber === 4) {
  scoreType = 'score-4'
} else if (randomNumber === 6) {
  scoreType = 'score-6'
} else {
  scoreType = 'score-8'
}

let isTwoCardPresend = false
let pairCounter = 0
// TODO: Implement this function!
function handleCardClick(event) {
  // you can use event.target to see which element was clicked
  if (isTwoCardPresend) {
    return
  }
  event.target.classList.add('cardOpen')
  if (cardOpen.length <= 2) {
    let elementClass = event.target.getAttribute('class').split(' ')
    let color = elementClass[1]
    flipCard(event.target, color)
    const counter = Number(turns.innerText)
    const colors = (randomGifsArray.length) / 2
    if (cardOpen.length === 2) {
      isTwoCardPresend = true
      setTimeout(() => {
        if (cardOpen[0].style.background === cardOpen[1].style.background) {
          turns.innerText = counter + 1
          cardOpen[0].removeEventListener('click', handleCardClick)
          cardOpen[1].removeEventListener('click', handleCardClick)
          cardOpen[1].classList.remove("cardOpen")
          cardOpen[0].classList.remove("cardOpen")
          pairCounter++
          if (pairCounter === colors) {
            gameSection.style.display = 'none'
            gameTurns.style.display = 'none'
            bestSocreStatus.style.display='none'
            reset.style.display = 'inline'
            const lowestMove = localStorage.getItem(scoreType)
            if (lowestMove > counter) {
              localStorage.setItem(scoreType, Number(turns.innerText))
              winTag.innerText = `You set new score in ${turns.innerText} moves`
            } else {
              winTag.innerText = `You won in ${turns.innerText} moves \n best score is ${lowestMove} moves`
            }
            winTag.style.display = 'block'
          }
          isTwoCardPresend = false
        } else {
          turns.innerText = counter + 1
          returnCardOriginalPosition(cardOpen[0])
          returnCardOriginalPosition(cardOpen[1])
          cardOpen[1].classList.remove("cardOpen")
          cardOpen[0].classList.remove("cardOpen")
          isTwoCardPresend = false
        }
      }, 1 * 1000)
    }
  }
}
function flipCard(card, color) {
  card.classList.add("rotate")
  card.style.background = color
  card.style.backgroundRepeat='no-repeat'
  card.style.backgroundSize='100% 100%'
}
function returnCardOriginalPosition(card) {
  card.classList.remove("rotate")
  card.style.background = "#20130B"
  card.style.backgroundImage='url(resource/question-mark.png)'
  card.style.backgroundSize='100% 100%'
}


reset.addEventListener('click', () => {
  const lowestMove = localStorage.getItem(scoreType)
  if(lowestMove==Infinity){
    bestSocre.innerText='NA'
  }else{
    bestSocre.innerText=lowestMove
  }
  turns.innerText = 0
  reset.style.display = 'none'
  while (gameContainer.firstChild) {
    gameContainer.firstChild.remove()
  }
  randomNumber = randomNumberOfCard()
  randomGifsArray = generateRandomNumberOfGifs(randomNumber)
  shuffledColors = shuffle(randomGifsArray)
  createDivsForColors(shuffledColors)
  if (randomNumber === 4) {
    scoreType = 'score-4'
  } else if (randomNumber === 6) {
    scoreType = 'score-6'
  } else {
    scoreType = 'score-8'
  }
  pairCounter = 0
  gameSection.style.display = 'flex'
  gameTurns.style.display = 'inline'
  bestSocreStatus.style.display='inline'
  winTag.style.display = 'none'
})

function handleStartClick(event) {
  const lowestMove = localStorage.getItem(scoreType)
  if(lowestMove==Infinity){
    bestSocre.innerText='NA'
  }else{
    bestSocre.innerText=lowestMove
  }
  turnStatus.style.display = 'inline'
  bestSocreStatus.style.display= 'inline'
  gameSection.style.display = 'flex'
  start.style.display = 'none'
  instruction.style.display='none'
  start.removeEventListener('click', handleStartClick)
}

start.addEventListener('click', handleStartClick)

// when the DOM loads
createDivsForColors(shuffledColors)